<?php

use App\Http\Controllers\basket\BasketController;
use App\Http\Controllers\categorie\CategorieController;
use App\Http\Controllers\categorie\SubCategorieController;
use App\Http\Controllers\country\CountryController;
use App\Http\Controllers\home\HomeController;
use App\Http\Controllers\login\ConnexionController;
use App\Http\Controllers\product\ProductController;
use App\Http\Controllers\roles\RoleController;
use App\Http\Controllers\shop\shopController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('website.home');
    // Ca doit etre l'accueil
})->name('welcome');
*/
Route::get('/',[HomeController::class,'index'])->name('welcome');
Route::get('/shop/{shopName}/{id}',[shopController::class,'index'])->name('seller');
Route::get('/product/{id}',[productController::class,'show'])->name('productindex');
Route::get('/login', function (Request $request) {
    $token = $request->session()->get('token');
    if ($token != null) {
        $basket = $request->session()->get('basket');
        $productBasket = $request->session()->get('productBasket');
    } else {
        $basket = [];
       $productBasket = [];
    }
    return view('website.login',compact('basket','productBasket','token'));
})->name('login');
Route::post('/login',[HomeController::class,'connexion'])->name('userlogin');
Route::get('/logout',[ConnexionController::class,'logout'])->name('userlogout');

Route::get('/listproduit', [ProductController::class,'index'])->name('listProduit');

Route::get('/boutique', [shopController::class,'showShop'])->name('showShop');
Route::post('basket/create',[BasketController::class,'store'] )->name('addBasket');
Route::get('addShop', function (Request $request) {
    $infoUser = $request->session()->get('infoUser');

    $infoShop = $request->session()->get('shop');
    return view('admin.gestionShop.addShop' , compact('infoUser' , 'infoShop'));
})->name('createShop');
Route::post('/addShop',[shopController::class,'store'])->name('addingShop');
Route::post('/addUser',[HomeController::class,'store'])->name('addingUser');
Route::post('/sendCodeOtp',[HomeController::class,'codeOtp'])->name('sendCodeOtp');
Route::get('/addProduct', [ProductController::class,'addProduct'])->name('createProduct');
Route::post('/roles',[ProductController::class,'store'])->name('addingProduct');


/*
Route::get('/seller', function () {
    return view('website.seller');
    // Ca doit etre l'accueil
});
Route::get('/product', function () {
    return view('website.product_detail');
    // Ca doit etre l'accueil
});
*/
Route::get('/exploration', function () {
    return view('website.exploration');
    // Ca doit etre l'accueil
});


Route::get('/confimration', function () {
    return view('website.confirmation');
    // Ca doit etre l'accueil
});


Route::get('/basket', function (Request $request) {
    $token = $request->session()->get('token');
    if ($token != null) {
        $basket = $request->session()->get('basket');
        $productBasket = $request->session()->get('productBasket');
    } else {
        $basket = [];
       $productBasket = [];
    }
    return view('website.basket',compact('basket','productBasket','token'));
    // Ca doit etre l'accueil
})->name('seeBasket');


Route::get('/checkout', function () {
    return view('website.checkout');
    // Ca doit etre l'accueil
});






Route::get('/product', function () {
    return view('website.product_detail');
    // Ca doit etre l'accueil
});

Route::get('/register', function (Request $request) {
    $token = $request->session()->get('token');
    if ($token != null) {
        $basket = $request->session()->get('basket');
        $productBasket = $request->session()->get('productBasket');
    } else {
        $basket = [];
       $productBasket = [];
    }
    return view('website.register',compact('basket','productBasket','token'));

})->name('register');

Route::get('/codeOtp', function (Request $request) {
    $token = $request->session()->get('token');
    if ($token != null) {
        $basket = $request->session()->get('basket');
        $productBasket = $request->session()->get('productBasket');
    } else {
        $basket = [];
       $productBasket = [];
    }
    return view('website.submitCodeOtp',compact('basket','productBasket','token'));

})->name('writeOTP');
Route::get('/dashboard', function (Request $request) {
    $infoUser = $request->session()->get('infoUser');

    $infoShop = $request->session()->get('shop');

    return view('admin.home.home',compact('infoUser','infoShop'));
    // Ca doit etre l'accueil
})->name('dashboard');

/*
Route::post('/login',[ConnexionController::class,'login'])->name('login');
Route::get('/listPays',[CountryController::class,'index'])->name('listPays');
Route::post('/pays',[CountryController::class,'store'])->name('addCountry');
Route::get('/listRole', [RoleController::class,'index'])->name('listRole');;
Route::post('/roles',[RoleController::class,'store'])->name('addRole');
Route::get('/listCategorie', [CategorieController::class,'index'])->name('listCategorie');;
Route::post('/categorie', [CategorieController::class,'store'])->name('addCategorie');;
Route::get('/listSousCategorie', [SubCategorieController::class,'index'])->name('listSousCategorie');;
Route::post('/subcategorie', [SubCategorieController::class,'store'])->name('addsubCategorie');;

Route::post('/updatePassword', [ConnexionController::class,'updatePassword'])->name('updatePassword');;


;

Route::get('/listAdministrateur', function () {
    return view('gestionAdmin.listAdmin');
    // Ca doit etre l'accueil
})->name('listAdministrateur');

Route::get('/addAdministrateur', function () {
    return view('gestionAdmin.addAdmin');
    // Ca doit etre l'accueil
})->name('addAdministrateur');;






Route::get('/search', function () {
    return view('search.search');
    // Ca doit etre l'accueil
})->name('search');;

Route::get('/listUtilisateur', function () {
    return view('gestionUtilisateurs.listUtilisateur');
    // Ca doit etre l'accueil
})->name('listUtilisateur');;

Route::get('/listvendeur', function () {
    return view('gestionUtilisateurs.listVendeur');
    // Ca doit etre l'accueil
})->name('listVendeur');;



Route::get('/profile', function () {
    return view('parametre.profile');
    // Ca doit etre l'accueil
})->name('profile');
*/
