<div class="container-menu-desktop trans-03">
    <div class="wrap-menu-desktop">
        <nav class="limiter-menu-desktop p-l-45">
           <?php $siteCategorie = Cache::get('categorie'); ?>


            <!-- Logo desktop -->
            <a href="#" class="logo">
                <img src="{{asset('images/icons/OpenTrade.jpeg')}} " alt="IMG-LOGO" >
            </a>

            <!-- Menu desktop  -->
            <div class="menu-desktop">
                <ul class="main-menu">
                    <li>
                        <a href="{{route('welcome')}}">Accueil</a>

                    </li>


                    <li>
                        <a href="Exploration.html">Explorer</a>
                    </li>

                    <li>
                        <a href="#">Categories</a>
                        <ul class="sub-menu">
                            @foreach ($siteCategorie as $siteCategorie)
                            <li><a href="#">{{$siteCategorie->libelle}}</a></li>
                            @endforeach


                        </ul>
                    </li>





                    @if ($token!= null)
                    <li>
                        <a href="{{route('userlogout')}}">Se deconnecter</a>
                    </li>
                    @else
                    <li>
                        <a href="{{route('login')}}">Se connecter</a>
                    </li>
                    <li>
                        <a href="{{route('writeOTP')}}">Valider compte</a>
                    </li>
                    <li>
                        <a href="{{route('register')}}">S'enregistrer</a>
                    </li>
                    @endif



                </ul>
            </div>

            <!-- Icon header -->
            <div class="wrap-icon-header flex-w flex-r-m">


                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify={{count($productBasket)}}>
                    <i class="zmdi zmdi-shopping-cart"></i>
                </div>

            </div>
        </nav>
    </div>
</div>

<div class="wrap-header-mobile">
    <!-- Logo moblie -->
    <div class="logo-mobile">
        <a href="#"><img src="{{asset('images/icons/OpenTrade.jpeg')}}" alt="IMG-LOGO"></a>
    </div>

    <!-- Icon header -->
    <div class="wrap-icon-header flex-w flex-r-m h-full m-r-15">
        <div class="flex-c-m h-full p-r-5">
            <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify={{count($productBasket)}}>
                <i class="zmdi zmdi-shopping-cart"></i>
            </div>
        </div>
    </div>

    <!-- Button show menu -->
    <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
    </div>
</div>


<!-- Menu Mobile -->
<div class="menu-mobile">
    <ul class="main-menu-m">
        <li>
            <?php $siteCategorie = Cache::get('categorie'); ?>
                        <a href="{{route('welcome')}}">Accueil</a>

                    </li>

                    <li>
                        <a href="Exploration.html">Explorer</a>
                    </li>

                    <li>
                        <a href="#">Categories</a>
                        <ul class="sub-menu">
                            @foreach ($siteCategorie as $siteCategorie)
                            <li><a href="#">{{$siteCategorie->libelle}}</a></li>
                            @endforeach


                        </ul>
                    </li>





                    @if ($token!= null)
                    <li>
                        <a href="{{route('userlogout')}}">Se deconnecter</a>
                    </li>
                    @else
                    <li>
                        <a href="{{route('login')}}">Se connecter</a>
                    </li>
                    <li>
                        <a href="{{route('writeOTP')}}">Valider compte</a>
                    </li>
                    <li>
                        <a href="{{route('register')}}">S'enregistrer</a>
                    </li>
                    @endif


    </ul>
</div>

<!-- Modal Search -->
<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
    <button class="flex-c-m btn-hide-modal-search trans-04">
        <i class="zmdi zmdi-close"></i>
    </button>

    <form class="container-search-header">
        <div class="wrap-search-header">
            <input class="plh0" type="text" name="search" placeholder="Search...">

            <button class="flex-c-m trans-04">
                <i class="zmdi zmdi-search"></i>
            </button>
        </div>
    </form>
</div>
