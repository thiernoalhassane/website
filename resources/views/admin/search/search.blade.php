@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Recherche
        </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Recherche</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<h2 class="text-center display-4">Lancer une recherche</h2>
            <form action="enhanced-results.html">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Type de resultat:</label>
                                    <select id="inputStatus" class="form-control custom-select">
                                        <option selected disabled>Choix du resultat</option>
                                        <option>produit</option>
                                        <option>categorie</option>
                                        <option>sous-categorie</option>
                                        <option>administrateur</option>
                                        <option>utilisateur</option>
                                        <option>vendeur</option>
                                      </select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Tirer par ordre:</label>
                                    <select id="inputStatus" class="form-control custom-select">
                                        <option selected disabled>Choix du role</option>
                                        <option>ASC</option>
                                        <option>DESC</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <input type="search" class="form-control form-control-lg" placeholder="Type your keywords here" >
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-lg btn-default">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
@endsection


@section('js')
<script>
    <!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../../plugins/select2/js/select2.full.min.js"></script>
    $(function () {
      $('.select2').select2()
    });
</script>
@endsection


