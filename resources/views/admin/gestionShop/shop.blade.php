@extends('admin.template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Boutique </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Boutique</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<div class="row">
    <div class="col-md-5">

      <!-- Profile Image -->
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                 src="../administrateur/dist/img/user.png"
                 alt="Shop picture">
          </div>

          <h3 class="profile-username text-center">{{$infoShop->name}}</h3>

          <p class="text-muted text-center">{{$infoUser->country->name}}</p>

          <ul class="list-group list-group-unbordered mb-3">
            <li class="list-group-item">
              <b>Proprietaire</b> <a class="float-right">{{$infoUser->firstName}} {{$infoUser->lastName}}</a>
            </li>
            <li class="list-group-item">
              <b>Nombre de produit</b> <a class="float-right">{{count($product)}}</a>
            </li>
            <li class="list-group-item">
              <b>Nombre de commande</b> <a class="float-right"></a>
            </li>
          </ul>
          <ul class="list-group  mb-3">
            <li class="list-group-item">
                <a href="{{route('createProduct')}}">
                    <button type="submit"  class="btn btn-primary">Ajouter un produit</button>
                </a>
                <button type="submit" class="btn btn-primary">Liste des commandes</button>
            </li>
            <li class="list-group-item">

                <button type="submit" class="btn btn-primary">Modifier information boutique</button>
            </li>

          </ul>



        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

      <!-- About Me Box -->

      <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-7">
        <!-- /.card -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Liste des comandes</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nom Produit</th>
                <th>Quantite</th>

              </tr>
              </thead>
              <tbody>


              </tbody>
              <tfoot>
              <tr>
                  <th>Nom Produit</th>
                  <th>Quantite</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    <!-- /.col -->
</div>

<div class="col-12">
    <!-- /.card -->

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Liste des produits</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example2" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Nom Produit</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($product as $product)
            <tr>
                <td>{{$product->libelle}}</td>
                <td>
                </td>
            </tr>
        @endforeach

          </tbody>
          <tfoot>
          <tr>
              <th>Nom Produit</th>
              <th>Actions</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  @endsection

  @section('js')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $("#example2").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
    });
</script>
@endsection









