@extends('admin.template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Ajouter une boutique</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Ajouter une  boutique</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
<form action="{{route('addingShop')}}" method="POST">
    @csrf
    <div class="col-lg-12">

        <div class="form-panel">
          <div class=" form">
            <form class="cmxform form-horizontal style-form" id="commentForm" >
            @csrf
              <div class="form-group ">
                <label for="cname" class="control-label col-lg-2">Nom de la boutique (obligatoire)</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="name" minlength="2" type="text" required />
                </div>
              </div>

              <div class="form-group ">
                <label for="cname" class="control-label col-lg-2">Contact de la boutique (obligatoire)</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="number" minlength="2" type="text" required />
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-theme" type="submit">Valider</button>
                  <button class="btn btn-theme04" type="Cancel">Annuler</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /form-panel -->
      </div>@endsection
