@extends('template.generique')

@section('contenuHeader')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0">Liste des roles <span class="badge badge-info right">{{count($role)-1}}</span>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                Ajouter un role
            </button>
        </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Liste des roles</li>
        </ol>
        </div><!-- /.col -->
    </div>
@endsection

@section('contenu')
    <div class="row">
        @foreach ($role as $role)
            @if ($role->name != "Client")
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                    <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0">
                            {{$role->name}}
                            <br>
                            <br>
                            <a href="#" class="btn btn-sm btn-danger">
                                Supprimer
                            </a>
                            <a href="#" class="btn btn-sm btn-primary">
                                Changer le nom
                            </a>
                        </div>
                    </div>
                </div>
            @endif

        @endforeach

    </div>

@endsection

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <form action="{{route('addRole')}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Ajout d'un role</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputName">Nom du role</label>
                        <input type="text" id="inputName" name="role" class="form-control">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                <button type="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </div>
        </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

