<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait RestApi
{

    protected function url()
    {
        $url = "http://api.africancommunitymarkets.com/api/v1/";
        return $url;
    }

    protected function loginurl()
    {
        $lien = 'login';
        $url = $this->url().$lien;
        return $url;
    }

    protected function logouturl()
    {
        $lien = 'logout';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getCountry()
    {
        $lien = 'administrators/country/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addTobasket()
    {
        $lien = 'basket/addProduct';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addClient()
    {
        $lien = 'client/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function submitOtp()
    {
        $lien = 'submitCodeOtp';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getAllbasket()
    {
        $lien = 'basket/getProduct';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getSellerProduct()
    {
        $lien = 'seller/product/getAll';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addCountry()
    {
        $lien = 'administrators/country/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getRoles()
    {
        $lien = 'administrators/roles/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getAllShop()
    {
        $lien = 'shopall';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addRoles()
    {
        $lien = 'administrators/roles/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getCategorie()
    {
        $lien = 'categories/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addCategorie()
    {
        $lien = 'administrators/categories/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getsubCategorie()
    {
        $lien = 'subcategories/all';
        $url = $this->url().$lien;
        return $url;
    }

    protected function addsubCategorie()
    {
        $lien = 'administrators/subcategories/add';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getDistinctProduct()
    {
        $lien = 'product/getDistinctProduct';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getShop($id)
    {
        $lien = 'shop/getProduct/'.$id;
        $url = $this->url().$lien;
        return $url;
    }

    protected function getShopByCountry()
    {
        $lien = 'shop/country';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getDashboard()
    {
        $lien = 'client/dashboard';
        $url = $this->url().$lien;
        return $url;
    }

    public function getInformation($id)
    {
        $lien = 'product/getInformation/'.$id;
        $url = $this->url().$lien;
        return $url;
    }


    protected function getShopPromotionProduct($id)
    {
        $lien = 'shop/getShopPromotionProduct/'.$id;
        $url = $this->url().$lien;
        return $url;
    }

    protected function getShopNewProduct($id)
    {
        $lien = 'shop/getShopNewProduct/'.$id;
        $url = $this->url().$lien;
        return $url;
    }

    protected function getDistinctPromotionProduct()
    {
        $lien = 'product/getDistinctPromotionProduct';
        $url = $this->url().$lien;
        return $url;
    }

    protected function getDistinctNewProduct()
    {
        $lien = 'product/getDistinctNewProduct';
        $url = $this->url().$lien;
        return $url;
    }

    protected function productaddingurl()
    {
        $lien = 'seller/product/add';
        $url = $this->url().$lien;
        return $url;
    }




    protected function updateUserPassword()
    {
        $lien = 'updatePassword';
        $url = $this->url().$lien;
        return $url;
    }

    public function createShop()
    {
        $lien = 'shop/add';
        $url = $this->url().$lien;
        return $url;

    }







}
