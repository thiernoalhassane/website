<?php

namespace App\Http\Controllers\categorie;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CategorieController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $this->getCategorie();
        $response = Http::acceptJson()->get($url);
            $res = json_decode($response->body()) ;
            $categorie= $res->data;
            return view('gestionCategorie.listCategorie',compact('categorie'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $url = $this->addCategorie();
        $form_request = [
            'libelle'=> $request["libelle"],
        ];

            $response = Http::acceptJson()->post($url,$form_request);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                return redirect()->to('listCategorie');
            } else {
                return redirect()->back();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
