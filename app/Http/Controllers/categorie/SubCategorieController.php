<?php

namespace App\Http\Controllers\categorie;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SubCategorieController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $this->getCategorie();
        $urlsecond = $this->getsubCategorie();
        $response = Http::acceptJson()->get($url);
        $secondresponse = Http::acceptJson()->get($urlsecond);
            $res = json_decode($response->body()) ;
            $secondres = json_decode($secondresponse->body());
            $categorie= $res->data;
            $subcategorie = $secondres->data;

            return view('gestionCategorie.listSousCategorie',compact('categorie','subcategorie'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $url = $this->addsubCategorie();
        $form_request = [
            'libelle'=> $request["libelle"],
            'category'=> $request["category"],
        ];

            $response = Http::acceptJson()->post($url,$form_request);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                return redirect()->to('listSousCategorie');
            } else {
                return redirect()->back();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
