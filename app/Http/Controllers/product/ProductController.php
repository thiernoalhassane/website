<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = $this->getSellerProduct();
        $token = $request->session()->get('token');
        $response = Http::withToken($token)->acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        $product= $res->data;
        $infoUser = $request->session()->get('infoUser');

        $infoShop = $request->session()->get('shop');
        return view('admin.gestionUtilisateurs.listProduit',compact('product','infoUser','infoShop'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function addProduct(Request $request)
    {
        $infoUser = $request->session()->get('infoUser');

        $infoShop = $request->session()->get('shop');

        $urlsecond = $this->getsubCategorie();
        $secondresponse = Http::acceptJson()->get($urlsecond);
        $secondres = json_decode($secondresponse->body());
        $sousCategorie = $secondres->data;
        return view('admin.gestionAdmin.addProduit',compact("sousCategorie",'infoUser','infoShop'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = $this->productaddingurl();
        /*
        $body = [ "headers" => [
                "Accept" => "multipart/form-data"
              ],
              "multipart" => []
           ];
    $images = $request->file('image');
    if (is_array($images)) {
        foreach ($images as $image) {
            array_push($body["multipart"], ["name" => "image[]",
                "contents" => file_get_contents($image),
                "filename" => $image->getClientOriginalName()]);
        }
    }  pour gerer les arrays


    $images = $request->file('image');
    if (is_array($images)) {
        foreach ($images as $image) {
            array_push($body["multipart"], ["name" => "image[]",
                "contents" => file_get_contents($image),
                "filename" => $image->getClientOriginalName()]);
        }
    }

        */
        $image = $request->file('picture');
        $libelle =
        [
            'name'=>'libelle',
            'contents'=>$request["libelle"],
        ];
        $description = ['name'=>'description',
        'contents'=>$request["description"],];
        $stock = ['name'=>'stock',
        'contents'=>$request["quantity"],];

        $price = [ 'name'=>'price',
        'contents'=>$request["price"],];

        $picture = ['name'=>'picture',
        "contents" => file_get_contents($image),
        "filename" => $image->getClientOriginalName()];


        $body = [];
        foreach ($request["subCategorie"] as $subCategory) {
            array_push($body, ["name" => "subCategory[]",
                "contents" => $subCategory,
                ]);
        }
        /*
        foreach ($request->file('picture') as $image) {
            array_push($body, ["name" => "picture[]",
                "contents" => file_get_contents($image),
                "filename" => $image->getClientOriginalName()
            ]);
        }
        */
        array_push($body,$libelle);
        array_push($body,$description);
        array_push($body,$stock);
        array_push($body,$price);
        array_push($body,$picture);
       // dd($body);

       $token = $request->session()->get('token');
        $response = Http::withToken($token)->acceptJson();
        $result = $response->attach('picture',$request->file('picture'))->post($url,$body);

        $res = json_decode($result->body()) ;
        if ($res->status == "success") {
                return redirect()->to('boutique');
        } else {
                return redirect()->back();
        }

        //Http::attach()







    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $token = $request->session()->get('token');
        if ($token != null) {
            $basket = $request->session()->get('basket');
            $productBasket = $request->session()->get('productBasket');
        } else {
            $basket = [];
           $productBasket = [];
        }
        $url = $this->getInformation($id);
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        $product= $res->data;
        return view('website.product_detail',compact('product','token','basket','productBasket'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
