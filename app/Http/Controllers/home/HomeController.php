<?php

namespace App\Http\Controllers\home;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $url = $this->getCategorie();
        $urlsecond = $this->getsubCategorie();


        $response = Http::acceptJson()->get($url);
        $secondresponse = Http::acceptJson()->get($urlsecond);


        $res = json_decode($response->body()) ;
        $secondres = json_decode($secondresponse->body());


        $categorie= $res->data;
        $subcategorie = $secondres->data;

        Cache::put('categorie', $categorie);



        $token = $request->session()->get('token');






        if ($token != null) {
            $urlthird = $this->getDashboard();
            $urlfourth = $this->getShopByCountry();

            $basket = $request->session()->get('basket');
            $productBasket = $request->session()->get('productBasket');
            $responsethird = Http::withToken($token)->acceptJson()->get($urlthird);
            $responsefourth = Http::withToken($token)->acceptJson()->get($urlfourth);


            $resthird = json_decode($responsethird->body()) ;
            $restfourth = json_decode($responsefourth->body()) ;
            //dd($restfourth);
            $dashboard= $resthird->data;
            $shop = $restfourth->data;



            return view('website.Userhome',compact('categorie','subcategorie','dashboard','shop','basket','productBasket','token'));

        } else {
            $urlthird = $this->getAllShop();
            $urlfourth = $this->getDistinctProduct();
            $urlfive = $this->getDistinctPromotionProduct();
            $urlsix = $this->getDistinctNewProduct();
            $basket = [];
            $productBasket = [];

            $thirdresponse = Http::acceptJson()->get($urlthird);
            $fourthresponse = Http::acceptJson()->get($urlfourth);
            $fiveresponse = Http::acceptJson()->get($urlfive);
            $sixresponse = Http::acceptJson()->get($urlsix);
            $thirdres = json_decode($thirdresponse->body());
            $fourthres = json_decode($fourthresponse->body());
            $fiveres = json_decode($fiveresponse->body());
            $sixres = json_decode($sixresponse->body());
            $shop = $thirdres->data;
            $product = $fourthres->data;
            $promotionproduct = $fiveres->data;
            $newproduct = $sixres->data;

           



            return view('website.home',compact('categorie','subcategorie','shop','product','promotionproduct','newproduct','basket','productBasket','token'));
        }





    }


    public function connexion(Request $request)
    {
        $url = $this->loginurl();

            $response = Http::acceptJson()->post($url,[
                'phone'=> $request["phone"],
                'password'=>$request["password"]
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                $request->session()->put('token', $res->data->token);
                $request->session()->put('infoUser', $res->data->user);
                $request->session()->put('role', $res->data->role[0]);
                $request->session()->put('shop', $res->data->user->shop);
                $request->session()->put('basket', $res->data->user->basket);
                $request->session()->put('productBasket', $res->data->productbasket);
                return redirect()->to('/');
            } else {
                return redirect()->back();
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $url = $this->addClient();

            $response = Http::acceptJson()->post($url,[
                'firstName'=> $request["firstName"],
                'lastName'=>$request["lastName"],
                'password'=> $request["password"],
                'country'=>$request["country"],
                'phone'=> $request["phone"],
                'email'=>$request["email"],
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                return redirect()->to('/codeOtp');
            } else {
                return redirect()->back();
            }
    }

    public function codeOtp(Request $request)
    {
        $url = $this->submitOtp();
        $code = $request["codenumber1"].$request["codenumber2"].$request["codenumber3"].$request["codenumber4"];

            $response = Http::acceptJson()->post($url,[
                'code'=>$code,

            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                $request->session()->put('token', $res->data->token);
                $request->session()->put('infoUser', $res->data->user);
                $request->session()->put('role', $res->data->role[0]);
                $request->session()->put('shop', $res->data->user->shop);
                $request->session()->put('basket', $res->data->user->basket);
                $request->session()->put('productBasket', $res->data->productbasket);
                return redirect()->to('/');
            } else {
                return redirect()->back();
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
