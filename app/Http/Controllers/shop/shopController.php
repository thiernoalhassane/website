<?php

namespace App\Http\Controllers\shop;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class shopController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($shopName , $id, Request $request)
    {
        //
        $token = $request->session()->get('token');
        if ($token != null) {
            $basket = $request->session()->get('basket');
            $productBasket = $request->session()->get('productBasket');

        } else {
            $basket = [];
           $productBasket = [];
        }
        $url = $this->getShop($id);
        $response = Http::acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        $product= $res->data;
        $url2 = $this->getShopPromotionProduct($id);
        $response2 = Http::acceptJson()->get($url2);
        $res2 = json_decode($response2->body()) ;
        $promotionproduct= $res2->data;
        $url3 = $this->getShopNewProduct($id);
        $response3 = Http::acceptJson()->get($url3);
        $res3 = json_decode($response3->body()) ;
        $newproduct= $res3->data;
        //dd( count($product)  );

        return view('website.seller',compact('shopName','product','promotionproduct','newproduct','basket','productBasket', 'token'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            $url = $this->createShop();
            $token = $request->session()->get('token');
            $response = Http::withToken($token)->acceptJson()->post($url,[
                'name'=> $request["name"],
                'number'=>$request["number"]
            ]);
            $res = json_decode($response->body()) ;
            if ($res->status == "success") {

                $request->session()->forget('shop');
                $request->session()->put('shop', $res->data);
                return redirect()->to('/dashboard');
            } else {
                return redirect()->back();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function showShop(Request $request)
    {
        $infoUser = $request->session()->get('infoUser');
        $infoShop = $request->session()->get('shop');
        $role  = $request->session()->get('role ');
        $url = $this->getSellerProduct();
        $token = $request->session()->get('token');
        $response = Http::withToken($token)->acceptJson()->get($url);
        $res = json_decode($response->body()) ;
        $product= $res->data;
        return view('admin.gestionShop.shop',compact('product','infoUser','infoShop','role'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
