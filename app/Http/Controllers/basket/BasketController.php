<?php

namespace App\Http\Controllers\basket;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BasketController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //dd($request->session->get('productBasket'));
        $url = $this->addTobasket();

            $token = $request->session()->get('token');
            $response = Http::withToken($token)->acceptJson()->post($url,[
                'product'=> $request["product"],
                'quantity'=>$request["quantity"]
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {

                $id_basket = $request->session()->get('basket');
                $url2 = $this->getAllbasket();
                $response2 = Http::withToken($token)->acceptJson()->get($url2);
                $res2 = json_decode($response2->body()) ;


                if ($id_basket == $res->data->basket_id) {

                    //faire des array_push du produit dans le basket
                    $request->session()->put('productBasket',$res2->data);
                } else {
                    $request->session()->forget('basket');
                    $request->session()->put('basket', $res->data->basket_id);
                    $request->session()->put('productBasket',$res2->data);

                }


                return redirect()->back();
            } else {
                return redirect()->back();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
