<?php

namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use App\Traits\RestApi;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class ConnexionController extends Controller
{
    use RestApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        $url = $this->loginurl();
        $form_request = [
            'phone'=> $request["phone"],
            'password'=>$request["passord"]
        ];

            $response = Http::acceptJson()->post($url,[
                'phone'=> $request["phone"],
                'password'=>$request["password"]
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                $request->session()->put('token', $res->data->token);
                $request->session()->put('infoUser', $res->data->user);
                $request->session()->put('role', $res->data->role[0]);
                return redirect()->to('dashboard');
            } else {
                return redirect()->back();
            }
    }

    public function updatePassword(Request $request)
    {
        $url = $this->updateUserPassword();

            $token = $request->session()->get('token');
            $response = Http::withToken($token)->acceptJson()->post($url,[
                'password'=> $request["password"],
                'oldPassword'=>$request["oldPassword"]
            ]);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                return redirect()->to('profile');
            } else {
                return redirect()->back();
            }

    }

    public function logout(Request $request)
    {
        $url = $this->logouturl();

            $token = $request->session()->get('token');
            $response = Http::withToken($token)->acceptJson()->post($url);
            $res = json_decode($response->body()) ;

            if ($res->status == "success") {
                $request->session()->forget('token');
                $request->session()->forget('infoUser');
                $request->session()->forget('role');
                $request->session()->forget('shop');
                $request->session()->forget('basket');
                $request->session()->forget('productBasket');
                return redirect()->to('/');
            } else {
                return redirect()->back();
            }

    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
